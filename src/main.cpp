#include "racingtftree_handle.h"

int main(int argc, char **argv) {

    ros::init(argc, argv, "racingtftree");
    RacingTfTreeHandle handle = RacingTfTreeHandle();
    handle.start();
    handle.sendStaticTransform();
    std::cout << "Static TF sended. " << std::endl;

    while(ros::ok()) {
        ros::spinOnce();
    }

    return 0;

}