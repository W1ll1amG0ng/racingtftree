#pragma once

#include "fsd_common_msgs/CarState.h"

#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <geometry_msgs/TransformStamped.h>
#include <ros/ros.h>

#include <string>

class RacingTfTreeHandle {

    public:

        RacingTfTreeHandle();

        void start();
        void sendStaticTransform();

        // Callbacks
        void stateCallBack(const fsd_common_msgs::CarState::ConstPtr& msg);

    private:

        ros::NodeHandle node_;

        //broadcasters
        tf2_ros::TransformBroadcaster br_car_world_;
        tf2_ros::StaticTransformBroadcaster br_rslidar_car_;
        tf2_ros::StaticTransformBroadcaster br_cog_car_;
        tf2_ros::StaticTransformBroadcaster br_imu_car_;
        tf2_ros::StaticTransformBroadcaster br_camera_car_;

        // transforms
        geometry_msgs::TransformStamped transform_car_world_;     // world  -->    car
        geometry_msgs::TransformStamped transform_imu_car_;       // car    -->    imu
        geometry_msgs::TransformStamped transform_rslidar_car_;   // car    -->    rslidar
        geometry_msgs::TransformStamped transform_cog_car_;       // car    -->    cog
        geometry_msgs::TransformStamped transform_camera_car_;    // car    -->    camera

        // Topics
        ros::Subscriber state_sub_;

        // Parameters
        std::string state_topic_;
        std::string world_frame_id_;
        std::string car_frame_id_;



        // private methods
        void setTransformBasis(geometry_msgs::TransformStamped &transform, const std::string &parent_frame, const std::string &child_frame) {

            transform.header.stamp = ros::Time::now();
            transform.header.frame_id = parent_frame;
            transform.child_frame_id = child_frame;

        }

        void setTranslation(geometry_msgs::TransformStamped &transform, const double x, const double y, const double z) {

            transform.transform.translation.x = x;
            transform.transform.translation.y = y;
            transform.transform.translation.z = z;

        }
        void setRotation(geometry_msgs::TransformStamped &transform, tf2::Quaternion &q) {

            transform.transform.rotation.x = q.x();
            transform.transform.rotation.y = q.y();
            transform.transform.rotation.z = q.z();
            transform.transform.rotation.w = q.w();

        }

        void loadPartParam(std::string part_name, geometry_msgs::TransformStamped &transform);

};